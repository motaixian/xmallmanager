package cn.xmallmanager.commons.models;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/11.
 */

public class EUDataGridResult {
    private long total;
    private List<?> rows;
    public long getTotal() {
        return total;
    }
    public void setTotal(long total) {
        this.total = total;
    }
    public List<?> getRows() {
        return rows;
    }
    public void setRows(List<?> rows) {
        this.rows = rows;
    }

}
