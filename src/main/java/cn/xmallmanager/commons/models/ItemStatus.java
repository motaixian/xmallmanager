package cn.xmallmanager.commons.models;

/**
 * Created by MoTaiXian on 2017/7/11.
 */
public enum ItemStatus {
    NORMAL("正常",(byte)1),INSTOCK("下架",(byte)2),
    DELETED("删除",(byte)3),SELLOUT("售罄",(byte)4);
    private String desc;
    private Byte code;

    private ItemStatus(String desc,Byte code){
        this.desc=desc;
        this.code=code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }
}
