package cn.xmallmanager.controllers;

import cn.xmallmanager.commons.models.RequestResult;
import cn.xmallmanager.models.ItemParamItem;
import cn.xmallmanager.services.ItemParamItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
@Controller
@RequestMapping("/item_param_item")
public class ItemParamItemController {

    @Autowired
    ItemParamItemService itemParamItemService;

    @GetMapping("/query/item/{itemId}")
    @ResponseBody
    public RequestResult getItemParamInfo(@PathVariable("itemId") Long itemID){

        String paramData=itemParamItemService.getItemParamItemByItemId(itemID);
        if(paramData!=null){
            Map<String,String> map=new HashMap<>();
            map.put("id",String.valueOf(itemID));
            map.put("paramData",paramData);
            return RequestResult.ok("success",map);
        }

        return RequestResult.error("fail to query item "+String.valueOf(itemID));

    }

}
