package cn.xmallmanager.controllers;

import cn.xmallmanager.commons.models.ItemStatus;
import cn.xmallmanager.commons.models.PageQueryResult;
import cn.xmallmanager.commons.models.RequestResult;
import cn.xmallmanager.commons.utils.IDUtil;
import cn.xmallmanager.models.Item;
import cn.xmallmanager.models.ItemDesc;
import cn.xmallmanager.models.ItemParamItem;
import cn.xmallmanager.services.GeneralItemService;
import cn.xmallmanager.services.ItemService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by MoTaiXian on 2017/7/11.
 */
@Controller
@RequestMapping("/item")
public class ItemController {
    private final Logger logger=LoggerFactory.getLogger(ItemController.class);
    @Autowired
    ItemService itemService;

    @Autowired
    GeneralItemService generalItemService;

    @RequestMapping(value = "/list",method = RequestMethod.GET)
    @ResponseBody
    public PageQueryResult<Item> getItemList(int page, int rows){
        logger.debug("链接访问~！");
        return itemService.getItemsByPage(page,rows);
    }

    @RequestMapping(value = "/reshelf",method = RequestMethod.POST)
    @ResponseBody
    public RequestResult doReshelf(long ids)
    {
        itemService.updateStatuByID(ids, ItemStatus.NORMAL);
        return RequestResult.ok("reshelf Success");
    }

    @RequestMapping(value = "/instock",method = RequestMethod.POST)
    @ResponseBody
    public RequestResult doInstock(long ids){
        itemService.updateStatuByID(ids, ItemStatus.INSTOCK);
        return RequestResult.ok("instock Success");
    }

    @RequestMapping(value ="/delete",method = RequestMethod.POST)
    @ResponseBody
    public RequestResult doDelete(long ids){
        itemService.updateStatuByID(ids, ItemStatus.DELETED);
        return RequestResult.ok("delete Success");
    }

    @PostMapping(value = "/update")
    @ResponseBody
    public RequestResult doUpdate(Object obj){

        return RequestResult.ok("update Success");
    }

    @RequestMapping(value = "/edit",method = RequestMethod.GET)
    public String doEdit(@RequestParam("_") long ids){
        return "item-edit";
    }

    @RequestMapping(value = "/save",method = RequestMethod.POST)
    @ResponseBody
    public RequestResult doGeneralItemSave(GeneralItemVo generalItemVo,@RequestParam("itemParams") String itemParams) throws Exception {
        logger.debug(generalItemVo.toString());
        logger.debug(itemParams);

        Item item =generalItemVo.buildItem();

        //Item的Id是ItemDesc的Id外键
        ItemDesc itemDesc=new ItemDesc();
        itemDesc.setItemId(item.getId());
        itemDesc.setItemDesc(generalItemVo.getDesc());
        itemDesc.setCreated(item.getCreated());
        itemDesc.setUpdated(item.getUpdated());

        ////Item的Id是ItemParamItem的Id外键
        ItemParamItem itemParamItem=new ItemParamItem();
        itemParamItem.setItemId(item.getId());
        itemParamItem.setParamData(itemParams);
        itemParamItem.setCreated(item.getCreated());
        itemParamItem.setUpdated(item.getUpdated());

        generalItemService.saveItemDetail(item,itemDesc,itemParamItem);

        return RequestResult.ok();
    }


    static class GeneralItemVo{
        private Long cid;
        private String barcode;
        private String title;
        private String sellPoint;
        private Double priceView;
        private Long price;
        private Integer num;
        private String image;
        private String desc;

        public Long getCid() {
            return cid;
        }

        public void setCid(Long cid) {
            this.cid = cid;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }


        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSellPoint() {
            return sellPoint;
        }

        public void setSellPoint(String sellPoint) {
            this.sellPoint = sellPoint;
        }

        public Double getPriceView() {
            return priceView;
        }

        public void setPriceView(Double priceView) {
            this.priceView = priceView;
        }

        public Long getPrice() {
            return price;
        }

        public void setPrice(Long price) {
            this.price = price;
        }

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public Item buildItem(){
            Date date=new Date();
            long id= IDUtil.genItemId();
            Item item =new Item();
            item.setId(id);
            item.setCid(this.cid);
            item.setBarcode(this.barcode);
            item.setImage(this.image);
            item.setPrice(this.price);
            item.setNum(this.num);
            item.setTitle(this.title);
            item.setSellPoint(this.sellPoint);
            item.setStatus(ItemStatus.NORMAL.getCode());
            item.setCreated(date);
            item.setUpdated(date);

            return item;
        }

        @Override
        public String toString() {
            return "GeneralItemVo{" +
                    "cid=" + cid +
                    ", barcode='" + barcode + '\'' +
                    ", title='" + title + '\'' +
                    ", sellPoint='" + sellPoint + '\'' +
                    ", priceView=" + priceView +
                    ", price=" + price +
                    ", num=" + num +
                    ", image='" + image + '\'' +
                    ", desc='" + desc + '\'' +
                    '}';
        }
    }


}
