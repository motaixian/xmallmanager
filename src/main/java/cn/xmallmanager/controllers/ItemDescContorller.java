package cn.xmallmanager.controllers;

import cn.xmallmanager.commons.models.RequestResult;
import cn.xmallmanager.services.ItemDescService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
@Controller
@RequestMapping("/item_desc")
public class ItemDescContorller {

    @Autowired
    ItemDescService itemDescService;

    @GetMapping("/query/item/{itemId}")
    @ResponseBody
    public RequestResult getItemDesc(@PathVariable("itemId") Long itemId){
        String itemDesc=itemDescService.getItemDescById(itemId);
        if(itemDesc!=null){
            return RequestResult.ok("success",itemDesc);
        }

        return RequestResult.error("fail to query "+String.valueOf(itemId));
    }


}
