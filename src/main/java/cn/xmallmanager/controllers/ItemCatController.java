package cn.xmallmanager.controllers;

import cn.xmallmanager.commons.models.EUTreeNode;
import cn.xmallmanager.models.ItemCat;
import cn.xmallmanager.services.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/12.
 */
@Controller
@RequestMapping("/item_cat")
public class ItemCatController {

    @Autowired
    ItemCatService itemCatService;

    @PostMapping("/list")
    @ResponseBody
    public List<EUTreeNode> getEUTreeNodeList(@RequestParam(value="id",defaultValue="0")Long parentId){
        List<ItemCat> itemCats=itemCatService.getListByParentID(parentId);
        List<EUTreeNode> euTreeNodeList=new ArrayList<>();
        for(ItemCat itemCat: itemCats)
        {
            EUTreeNode euTreeNode=new EUTreeNode();
            euTreeNode.setId(itemCat.getId());
            euTreeNode.setText(itemCat.getName());
            euTreeNode.setState(itemCat.getIsParent()?"closed":"open");
            euTreeNodeList.add(euTreeNode);
        }
        return euTreeNodeList;
    }

//    @PostMapping("/list")
//    public String getEUTreeNodeRoot(){
//        return "forward:/item_cat/list?id=0";
//    }


}
