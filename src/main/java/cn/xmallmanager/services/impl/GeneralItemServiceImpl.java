package cn.xmallmanager.services.impl;

import cn.xmallmanager.commons.utils.IDUtil;
import cn.xmallmanager.dao.ItemDescMapper;
import cn.xmallmanager.dao.ItemMapper;
import cn.xmallmanager.dao.ItemParamItemMapper;
import cn.xmallmanager.models.Item;
import cn.xmallmanager.models.ItemDesc;
import cn.xmallmanager.models.ItemParamItem;
import cn.xmallmanager.services.GeneralItemService;
import cn.xmallmanager.services.GeneralItemService;
import cn.xmallmanager.services.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
@Service
public class GeneralItemServiceImpl implements GeneralItemService {
    @Autowired
    ItemMapper itemMapper;
    @Autowired
    ItemDescMapper itemDescMapper;
    @Autowired
    ItemParamItemMapper itemParamItemMapper;

    @Override
    @Transactional(propagation= Propagation.REQUIRED)
    public void saveItemDetail(Item item, ItemDesc itemDesc, ItemParamItem itemParamItem) throws Exception {
        itemMapper.insert(item);
        itemDescMapper.insert(itemDesc);
        itemParamItemMapper.insert(itemParamItem);
    }
}
