package cn.xmallmanager.services.impl;

import cn.xmallmanager.dao.ItemParamItemMapper;
import cn.xmallmanager.models.ItemParamItem;
import cn.xmallmanager.models.ItemParamItemExample;
import cn.xmallmanager.services.ItemParamItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
@Service
public class ItemParamItemServiceImpl implements ItemParamItemService {
    @Autowired
    ItemParamItemMapper itemParamItemMapper;

    @Override
    public String getItemParamItemByItemId(Long id) {
        ItemParamItemExample itemParamItemExample=new ItemParamItemExample();
        ItemParamItemExample.Criteria criteria=itemParamItemExample.createCriteria();
        criteria.andItemIdEqualTo(id);
        List<ItemParamItem> itemParamItemList=itemParamItemMapper.selectByExampleWithBLOBs(itemParamItemExample);
        if(itemParamItemList!=null && itemParamItemList.size()>0){
            return itemParamItemList.get(0).getParamData();
        }

        return null;
    }
}
