package cn.xmallmanager.services.impl;

import cn.xmallmanager.commons.utils.FtpUtil;
import cn.xmallmanager.commons.utils.IDUtil;
import cn.xmallmanager.services.PictureService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by MoTaiXian on 2017/7/13.
 */
@Service
public class PictureServiceFTPImpl implements PictureService {

    @Value("${IMG_FTP_ADDRESS}")
    private String FTP_ADDRESS;
    @Value("${IMG_FTP_PORT}")
    private Integer FTP_PORT;
    @Value("${IMG_FTP_USERNAME}")
    private String FTP_USERNAME;
    @Value("${IMG_FTP_PASSWORD}")
    private String FTP_PASSWORD;
    @Value("${IMG_FTP_BASE_PATH}")
    private String FTP_BASE_PATH;
    @Value("${IMG_BASE_URL}")
    private String IMAGE_BASE_URL;

    @Override
    @Transactional(propagation= Propagation.REQUIRED)
    public Map uploadPicture(MultipartFile uploadFile) {
        Map<String,Object> resultMap = new HashMap<>();
        try {
            //生成一个新的文件名
            //取原始文件名
            String oldName = uploadFile.getOriginalFilename();
            //生成新文件名
            //UUID.randomUUID();
            String newName = IDUtil.genImageName();
            newName = newName + oldName.substring(oldName.lastIndexOf("."));
            //图片上传
            String imagePath = new DateTime().toString("/yyyy/MM/dd");
            boolean result = FtpUtil.uploadFile(FTP_ADDRESS, FTP_PORT, FTP_USERNAME, FTP_PASSWORD,
                    FTP_BASE_PATH, imagePath, newName, uploadFile.getInputStream());
            //返回结果
            if(!result) {
                resultMap.put("error", 1);
                resultMap.put("message", "文件上传失败");
                return resultMap;
            }
            resultMap.put("error", 0);
            resultMap.put("url", IMAGE_BASE_URL + imagePath + "/" + newName);
            return resultMap;

        } catch (Exception e) {
            resultMap.put("error", 1);
            resultMap.put("message", "文件上传发生异常");
            return resultMap;
        }
    }
}
