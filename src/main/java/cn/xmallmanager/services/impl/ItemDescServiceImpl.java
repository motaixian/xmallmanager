package cn.xmallmanager.services.impl;

import cn.xmallmanager.dao.ItemDescMapper;
import cn.xmallmanager.models.ItemDesc;
import cn.xmallmanager.models.ItemDescExample;
import cn.xmallmanager.services.ItemDescService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
@Service
public class ItemDescServiceImpl implements ItemDescService {

    @Autowired
    ItemDescMapper itemDescMapper;
    @Override
    public String getItemDescById(Long id) {
        ItemDescExample itemDescExample=new ItemDescExample();
        ItemDescExample.Criteria criteria=itemDescExample.createCriteria();
        criteria.andItemIdEqualTo(id);
        List<ItemDesc> itemDescList=itemDescMapper.selectByExampleWithBLOBs(itemDescExample);
        if(itemDescList!=null && itemDescList.size()>0){
            return itemDescList.get(0).getItemDesc();
        }

        return null;
    }
}
