package cn.xmallmanager.services.impl;

import cn.xmallmanager.dao.ItemCatMapper;
import cn.xmallmanager.models.ItemCat;
import cn.xmallmanager.models.ItemCatExample;
import cn.xmallmanager.services.ItemCatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/12.
 */

@Service
public class ItemCatServiceImpl implements ItemCatService {

    @Autowired
    private ItemCatMapper itemCatMapper;

    @Override
    public List<ItemCat> getListByParentID(long parentID) {
        ItemCatExample example =new ItemCatExample();
        ItemCatExample.Criteria criteria=example.createCriteria();
        criteria.andParentIdEqualTo(parentID);
        List<ItemCat> itemCats=itemCatMapper.selectByExample(example);

        return itemCats;
    }
}
