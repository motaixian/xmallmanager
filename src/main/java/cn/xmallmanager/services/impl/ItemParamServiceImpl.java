package cn.xmallmanager.services.impl;

import cn.xmallmanager.commons.models.PageQueryResult;
import cn.xmallmanager.dao.ItemParamMapper;
import cn.xmallmanager.models.ItemParam;
import cn.xmallmanager.models.ItemParamExample;
import cn.xmallmanager.services.ItemParamService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/12.
 */

@Service
public class ItemParamServiceImpl implements ItemParamService {
    @Autowired
    ItemParamMapper itemParamMapper;


    @Override
    public ItemParam getByCatId(Long CatId) {
        ItemParamExample itemParamExample = new ItemParamExample();
        ItemParamExample.Criteria criteria=itemParamExample.createCriteria();
        criteria.andItemCatIdEqualTo(CatId);
        List<ItemParam> itemParamList=itemParamMapper.selectByExampleWithBLOBs(itemParamExample);

        if(itemParamList!=null && itemParamList.size()>0){
            return itemParamList.get(0);
        }

        return null;
    }

    @Override
    @Transactional(propagation= Propagation.REQUIRED)
    public void save(ItemParam itemParam) throws Exception {
        itemParamMapper.insert(itemParam);
    }

    @Override
    public PageQueryResult getItemParamsByPage(int page, int records) {
        PageQueryResult<ItemParam> result=new PageQueryResult<>();
        ItemParamExample itemExample=new ItemParamExample();
        ItemParamExample.Criteria criteria=itemExample.createCriteria();
        PageHelper.startPage(page,records);
        List<ItemParam> query_list=itemParamMapper.selectByExampleWithBLOBs(itemExample);
        result.setRows(query_list);
        PageInfo<ItemParam> pageInfo = new PageInfo<>(query_list);
        result.setTotal(pageInfo.getTotal());
        return result;
    }
}
