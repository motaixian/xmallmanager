package cn.xmallmanager.services;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
public interface ItemDescService {
    public String getItemDescById(Long id);
}
