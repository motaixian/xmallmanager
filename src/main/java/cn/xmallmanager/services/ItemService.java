package cn.xmallmanager.services;

import cn.xmallmanager.commons.models.ItemStatus;
import cn.xmallmanager.commons.models.PageQueryResult;
import cn.xmallmanager.models.Item;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/11.
 */
public interface ItemService {
    void save(Item item);
    Item getById(long id);
    void deleteById(long id);
    void updateStatuByID(long id, ItemStatus status);
    PageQueryResult<Item> getItemsByPage(int page, int records);
}
