package cn.xmallmanager.services;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
public interface ItemParamItemService {
    String getItemParamItemByItemId(Long id);
}
