package cn.xmallmanager.services;

import cn.xmallmanager.commons.models.PageQueryResult;
import cn.xmallmanager.models.ItemParam;

/**
 * Created by MoTaiXian on 2017/7/12.
 */
public interface ItemParamService {
    ItemParam getByCatId(Long CatId);
    void save(ItemParam itemParam) throws Exception;
    PageQueryResult getItemParamsByPage(int page,int records);
}
