package cn.xmallmanager.services;

import cn.xmallmanager.models.Item;
import cn.xmallmanager.models.ItemDesc;
import cn.xmallmanager.models.ItemParam;
import cn.xmallmanager.models.ItemParamItem;

/**
 * Created by MoTaiXian on 2017/7/14.
 */
public interface GeneralItemService {
    void saveItemDetail(Item item, ItemDesc itemDesc, ItemParamItem itemParamItem) throws Exception;
}
