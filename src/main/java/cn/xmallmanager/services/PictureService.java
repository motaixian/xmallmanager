package cn.xmallmanager.services;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * Created by MoTaiXian on 2017/7/13.
 */
public interface PictureService {
    public Map uploadPicture(MultipartFile uploadFile);
}
