package cn.xmallmanager.dao;

import cn.xmallmanager.models.ItemParamItem;
import cn.xmallmanager.models.ItemParamItemExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;


public interface ItemParamItemMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    long countByExample(ItemParamItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    int deleteByExample(ItemParamItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    int insert(ItemParamItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    int insertSelective(ItemParamItem record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    List<ItemParamItem> selectByExampleWithBLOBs(ItemParamItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    List<ItemParamItem> selectByExample(ItemParamItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    int updateByExampleSelective(@Param("record") ItemParamItem record, @Param("example") ItemParamItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    int updateByExampleWithBLOBs(@Param("record") ItemParamItem record, @Param("example") ItemParamItemExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tb_item_param_item
     *
     * @mbg.generated Mon Jul 10 23:19:31 CST 2017
     */
    int updateByExample(@Param("record") ItemParamItem record, @Param("example") ItemParamItemExample example);
}