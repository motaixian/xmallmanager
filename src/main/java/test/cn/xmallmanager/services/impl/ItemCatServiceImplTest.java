package test.cn.xmallmanager.services.impl; 

import cn.xmallmanager.models.ItemCat;
import cn.xmallmanager.services.ItemCatService;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/** 
* ItemCatServiceImpl Tester. 
* 
* @author <Authors name> 
* @since <pre>???? 12, 2017</pre> 
* @version 1.0 
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:D:/IdeaProject/work/xmallmanager/src/main/webapp/WEB-INF/applicationContext.xml")
public class ItemCatServiceImplTest { 

    @Autowired
    ItemCatService itemCatService;

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getListByParentID(long parentID) 
* 
*/ 
@Test
public void testGetListByParentID() throws Exception {
    List<ItemCat> itemCats=itemCatService.getListByParentID(0);
    return;
} 


} 
