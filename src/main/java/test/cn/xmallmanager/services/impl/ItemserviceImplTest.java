package test.cn.xmallmanager.services.impl; 

import cn.xmallmanager.commons.models.ItemStatus;
import cn.xmallmanager.commons.models.PageQueryResult;
import cn.xmallmanager.models.Item;
import cn.xmallmanager.services.ItemService;
import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;
import java.util.Optional;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;


/**
* ItemserviceImpl Tester.
*
* @author <Authors name>
* @since <pre>七月 11, 2017</pre>
* @version 1.0
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "file:D:/IdeaProject/work/xmallmanager/src/main/webapp/WEB-INF/applicationContext.xml")
public class ItemserviceImplTest {

    @Autowired
    ItemService itemService;
@Before
public void before() throws Exception {
}

@After
public void after() throws Exception {
}

/**
*
* Method: save(Item item)
*
*/
@Test
public void testSave() throws Exception {
//TODO: Test goes here...
}

/**
*
* Method: deleteById(long id)
*
*/
@Test
public void testDeleteById() throws Exception {
//TODO: Test goes here...
}

/**
* Method: public Item getById(long id)
 */
@Test
public  void testGetById() throws Exception{
    Item result=itemService.getById(0);
    Optional<Item> opt=Optional.of(result);

}

/**
 * Method: public void updateStatuByID(long id, ItemStatus status)
 */
@Test
public void testUpdateStatuByID(){
    itemService.updateStatuByID(536563, ItemStatus.INSTOCK);
    System.out.println(itemService.getById(536563));
    itemService.updateStatuByID(536563,ItemStatus.NORMAL);
}

/**
*
* Method: getItemsByPage(int page, int records)
*
*/
@Test
public void testGetItemsByPage() throws Exception {

//TODO: Test goes here...
    PageQueryResult<Item> result =itemService.getItemsByPage(1,20);

    System.out.println(result);
} 


} 
